/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.sql.SQLException;


public class AccesoDatos {
    
    ConexionBD bd;
    
    public AccesoDatos(){
        bd = new ConexionBD();
        bd.conectar();
    }

    public int getSaldo(String user) {
        
        try {
            
            String consulta = "SELECT saldo FROM Jugador WHERE usuario = '%s'";
                
            String sql = String.format(consulta, user);
            this.bd.res =  this.bd.stat.executeQuery(sql);

                if(bd.res.next())
                    return bd.res.getInt("saldo");
                    
            
        }catch(SQLException ex){
                
            System.out.println("Error de Mysql.");       
        }
        return 0;
    
    }

    public void addApuesta(int saldo, int idJugador) {
        
        String consulta = "update Jugador set saldo = '%d' where idJugador = %d;";
        String sql = String.format(consulta, saldo, idJugador);
               
        try {
            bd.stat.execute(sql);
        } catch (SQLException ex) {
            
        }

    }
    
    public Object[] getAll(String user, int idJugador) {
        
        try {
            
            String consulta = "SELECT * FROM Jugador WHERE usuario = '%s' And idJugador = %s";
            String sql = String.format(consulta, user, idJugador);
            
            this.bd.res =  this.bd.stat.executeQuery(sql);

                if(bd.res.next()){
                    String usuario = bd.res.getString("usuario");
                    int saldo = bd.res.getInt("saldo");
                    String fechaReg = bd.res.getString("fechaRegistro");
                    int partidasGanadas = bd.res.getInt("partidasGanadas");
                    int totalPartidas = bd.res.getInt("totalPartidas");
                    
                    Object[] obj = {usuario, saldo, fechaReg, partidasGanadas, totalPartidas};
                    return obj;
                    
                }
            
        }catch(SQLException ex){
                
            System.out.println("Error de Mysql.");       
        }
        return null;
    
    }

    public void addTotal(int totalInt, int idJugador) {
        String consulta = "update Jugador set totalPartidas = %d where idJugador = %d;";
        String sql = String.format(consulta, totalInt, idJugador);
               
        try {
            bd.stat.execute(sql);
        } catch (SQLException ex) {
            
        }
    }

    public int getTotalP(String user) {
        try {
            
            String consulta = "SELECT totalPartidas FROM Jugador WHERE usuario = '%s'";
                
            String sql = String.format(consulta, user);
            this.bd.res =  this.bd.stat.executeQuery(sql);

                if(bd.res.next())
                    return bd.res.getInt("totalPartidas");
                    
            
        }catch(SQLException ex){
                
            System.out.println("Error de Mysql.");       
        }
        return 0;
    }

    public int getGanadas(String user) {
        try {
            
            String consulta = "SELECT partidasGanadas FROM Jugador WHERE usuario = '%s'";
                
            String sql = String.format(consulta, user);
            this.bd.res =  this.bd.stat.executeQuery(sql);

                if(bd.res.next())
                    return bd.res.getInt("partidasGanadas");
                    
            
        }catch(SQLException ex){
                
            System.out.println("Error de Mysql.");       
        }
        return 0;
    }

    public void addGanadas(int ganadas, int idJugador) {
        String consulta = "update Jugador set partidasGanadas = %d where idJugador = %d;";
        String sql = String.format(consulta, ganadas, idJugador);
               
        try {
            bd.stat.execute(sql);
        } catch (SQLException ex) {
            
        }
    }

    public void setNewUser(String nombre, String apellido, String user, String pass, String email, int saldo) {
        String consulta = "INSERT INTO jugador (nombre, apellido, email, saldo, usuario, pass, fechaRegistro) VALUES ('%s', '%s', '%s', '%d', '%s', '%s', now())";
        String sql = String.format(consulta, nombre, apellido, email, saldo, user, pass);
        
        try {
            bd.stat.execute(sql);
        } catch (SQLException ex) {
            
        }
    }
        
}


